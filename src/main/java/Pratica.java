/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */

public class Pratica {
    public static void main(String[] args) {
        Runtime rt = Runtime.getRuntime();
        System.out.println(System.getProperties().get("os.name"));
        System.out.println( rt.availableProcessors() );
        System.out.println( rt.availableProcessors() );
        System.out.println( rt.totalMemory());
        System.out.println( rt.freeMemory() );
        System.out.println( rt.maxMemory() );
    }
}
